webpackJsonp(["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/ajout-recette/ajout-recette.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/ajout-recette/ajout-recette.component.html":
/***/ (function(module, exports) {

module.exports = "<form action=\"\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" >\n  <div class=\"form-group\">\n    <label for=\"nom\">Nom de la recette :</label>\n    <input type=\"text\" class=\"form-control\" id=\"nom\" name=\"nom\" [(ngModel)]=nouvelleRecette.nom>\n  </div>\n  <div class=\"form-group\">\n    <label for=\"difficulte\">Difficulté :</label>\n    <input type=\"number\" class=\"form-control\" id=\"difficulte\" name=\"difficulte\" [(ngModel)]=nouvelleRecette.difficulte>\n  </div>\n  <div class=\"form-group\">\n    <label for=\"origine\">Origine :</label>\n    <input type=\"text\" class=\"form-control\" id=\"origine\" name=\"origine\" [(ngModel)]=nouvelleRecette.origine>\n  </div>\n  <div class=\"form-group form-check\">\n    <label class=\"form-check-label\">\n      <input class=\"form-check-input\" type=\"checkbox\" name=\"vegan\" [(ngModel)]=nouvelleRecette.vegan> Vegan ?\n    </label>\n  </div>\n  <div class=\"form-group\" *ngFor=\"let i of array\">\n      <label for=\"ingredient\">Ingrédient :</label>\n      <input type=\"text\" class=\"form-control\" id=\"ingredient\" [name]=\"ingredient(i)\" [(ngModel)]=nouvelleRecette.ingredients[i].nom>\n      <label for=\"quantite\">Quantité :</label>\n      <input type=\"number\" class=\"form-control\" id=\"quantite\" [name]=\"quantite(i)\" [(ngModel)]=nouvelleRecette.ingredients[i].quantite>\n    </div>\n  <button class=\"btn btn-success\" (click)=\"addIngredient()\">+</button>\n  <div class=\"form-group\">\n      <label for=\"description\">Description de la recette :</label>\n      <textarea type=\"text\" style=\"height:300px;\" size=500 class=\"form-control\" id=\"description\" name=\"description\" [(ngModel)]=nouvelleRecette.description></textarea>\n    </div>\n  <button type=\"submit\" (click)=\"valide()\" class=\"btn btn-primary\">Submit</button>\n</form> "

/***/ }),

/***/ "./src/app/ajout-recette/ajout-recette.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AjoutRecetteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AjoutRecetteComponent = /** @class */ (function () {
    function AjoutRecetteComponent(requestService) {
        this.requestService = requestService;
        this.array = [0];
        this.validated = false;
        this.nouvelleRecette = { nom: "", difficulte: 0, origine: "", ingredients: [{ nom: "", quantite: 0 }], vegan: false, description: "" };
    }
    AjoutRecetteComponent.prototype.addIngredient = function () {
        this.array.push(this.array[this.array.length - 1] + 1);
        this.nouvelleRecette.ingredients.push({ nom: "", quantite: 0 });
    };
    AjoutRecetteComponent.prototype.ingredient = function (i) {
        return ("ingredient" + i);
    };
    AjoutRecetteComponent.prototype.quantite = function (i) {
        return ("quantite" + i);
    };
    AjoutRecetteComponent.prototype.valide = function () {
        this.validated = true;
    };
    AjoutRecetteComponent.prototype.onSubmit = function () {
        if (this.validated) {
            return this.requestService.ajouteRecette(this.nouvelleRecette).subscribe(function (data) {
            });
        }
    };
    AjoutRecetteComponent.prototype.ngOnInit = function () {
    };
    AjoutRecetteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-ajout-recette',
            template: __webpack_require__("./src/app/ajout-recette/ajout-recette.component.html"),
            styles: [__webpack_require__("./src/app/ajout-recette/ajout-recette.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */]])
    ], AjoutRecetteComponent);
    return AjoutRecetteComponent;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"text-align:center\">\n    <nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n        <ul class=\"navbar-nav\">\n          <li class=\"nav-item\" routerLinkActive=\"active\">\n            <a class=\"nav-link\" routerLink=\"recettes\">Liste des recettes</a>\n          </li>\n          <li class=\"nav-item\" routerLinkActive=\"active\">\n            <a class=\"nav-link\" routerLink=\"ajout\">Ajouter une recette</a>\n          </li>\n          <li class=\"nav-item\" routerLinkActive=\"active\">\n            <a class=\"nav-link\" routerLink=\"chat\">Chat Room</a>\n          </li>\n          <li class=\"nav-item\" routerLinkActive=\"active\" *ngIf=\"!logged\">\n            <a class=\"nav-link\" routerLink=\"auth\">Authentification</a>\n          </li>\n        </ul>\n        <app-recherche></app-recherche>\n      </nav>\n</div>\n<div class=\"container\">\n    <div class=\"row\">\n      <div>\n        <router-outlet></router-outlet>\n      </div>\n    </div>\n  </div>\n\n\n\n\n\n\n\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(requestService) {
        this.requestService = requestService;
        this.title = 'Mon livre de cuisine';
        this.nom = "";
        this.message = "";
    }
    AppComponent.prototype.generateLink = function () {
    };
    AppComponent.prototype.ngOnInit = function () {
        this.nom = this.requestService.user.nom;
        this.logged = this.requestService.loggedIn;
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-root',
            template: __webpack_require__("./src/app/app.component.html"),
            styles: [__webpack_require__("./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_recherche__ = __webpack_require__("./src/app/service/recherche.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__service_chat_service__ = __webpack_require__("./src/app/service/chat.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__service_token_interceptor__ = __webpack_require__("./src/app/service/token.interceptor.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular_font_awesome__ = __webpack_require__("./node_modules/angular-font-awesome/dist/angular-font-awesome.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__test_component_test_component_component__ = __webpack_require__("./src/app/test-component/test-component.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__recette_recette_component__ = __webpack_require__("./src/app/recette/recette.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__header_header_component__ = __webpack_require__("./src/app/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__recettes_affichage_recettes_affichage_component__ = __webpack_require__("./src/app/recettes-affichage/recettes-affichage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__recette_min_recette_min_component__ = __webpack_require__("./src/app/recette-min/recette-min.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__auth_auth_component__ = __webpack_require__("./src/app/auth/auth.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__service_auth_guard__ = __webpack_require__("./src/app/service/auth-guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ajout_recette_ajout_recette_component__ = __webpack_require__("./src/app/ajout-recette/ajout-recette.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__modifie_recette_modifie_recette_component__ = __webpack_require__("./src/app/modifie-recette/modifie-recette.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__recherche_recherche_component__ = __webpack_require__("./src/app/recherche/recherche.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__commentaires_commentaires_component__ = __webpack_require__("./src/app/commentaires/commentaires.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__chat_chat_component__ = __webpack_require__("./src/app/chat/chat.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var appRoutes = [
    { path: 'recettes', canActivate: [__WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_12__recettes_affichage_recettes_affichage_component__["a" /* RecettesAffichageComponent */], runGuardsAndResolvers: "always" },
    { path: "recettes/:recetteUrl", canActivate: [__WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_10__recette_recette_component__["a" /* RecetteComponent */], runGuardsAndResolvers: "always" },
    { path: "ajout", canActivate: [__WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_16__ajout_recette_ajout_recette_component__["a" /* AjoutRecetteComponent */] },
    { path: 'auth', component: __WEBPACK_IMPORTED_MODULE_14__auth_auth_component__["a" /* AuthComponent */] },
    { path: "chat", canActivate: [__WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_22__chat_chat_component__["a" /* ChatComponent */] },
    { path: '', canActivate: [__WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */]], component: __WEBPACK_IMPORTED_MODULE_12__recettes_affichage_recettes_affichage_component__["a" /* RecettesAffichageComponent */] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_9__test_component_test_component_component__["a" /* TestComponentComponent */],
                __WEBPACK_IMPORTED_MODULE_10__recette_recette_component__["a" /* RecetteComponent */],
                __WEBPACK_IMPORTED_MODULE_11__header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_12__recettes_affichage_recettes_affichage_component__["a" /* RecettesAffichageComponent */],
                __WEBPACK_IMPORTED_MODULE_13__recette_min_recette_min_component__["a" /* RecetteMinComponent */],
                __WEBPACK_IMPORTED_MODULE_14__auth_auth_component__["a" /* AuthComponent */],
                __WEBPACK_IMPORTED_MODULE_16__ajout_recette_ajout_recette_component__["a" /* AjoutRecetteComponent */],
                __WEBPACK_IMPORTED_MODULE_19__modifie_recette_modifie_recette_component__["a" /* ModifieRecetteComponent */],
                __WEBPACK_IMPORTED_MODULE_20__recherche_recherche_component__["a" /* RechercheComponent */],
                __WEBPACK_IMPORTED_MODULE_21__commentaires_commentaires_component__["a" /* CommentairesComponent */],
                __WEBPACK_IMPORTED_MODULE_22__chat_chat_component__["a" /* ChatComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_17__angular_router__["c" /* RouterModule */].forRoot(appRoutes, { onSameUrlNavigation: "reload" }),
                __WEBPACK_IMPORTED_MODULE_18__angular_common_http__["c" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_7_angular_font_awesome__["a" /* AngularFontAwesomeModule */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_3__service_request__["a" /* RequestService */],
                __WEBPACK_IMPORTED_MODULE_15__service_auth_guard__["a" /* AuthGuard */],
                __WEBPACK_IMPORTED_MODULE_4__service_recherche__["a" /* RechercheService */],
                __WEBPACK_IMPORTED_MODULE_5__service_chat_service__["a" /* ChatService */],
                {
                    provide: __WEBPACK_IMPORTED_MODULE_18__angular_common_http__["a" /* HTTP_INTERCEPTORS */],
                    useClass: __WEBPACK_IMPORTED_MODULE_6__service_token_interceptor__["a" /* TokenInterceptor */],
                    multi: true
                }
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth/auth.component.css":
/***/ (function(module, exports) {

module.exports = "p{\r\n    color:white;\r\n    font-weight: bold;\r\n}"

/***/ }),

/***/ "./src/app/auth/auth.component.html":
/***/ (function(module, exports) {

module.exports = "<form action=\"\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" *ngIf=\"!logged && !registering\" >\n  <div class=\"form-group\">\n    <label for=\"account\">Account :</label>\n    <input type=\"text\" class=\"form-control\" id=\"account\" name=\"account\" [(ngModel)]=user.account required>\n  </div>\n  <div class=\"form-group\">\n      <label for=\"password\">Password :</label>\n      <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=user.password required>\n    </div>\n    <button type=\"submit\" class=\"btn btn-primary\">Log in</button>\n</form>\n<div *ngIf=\"!logged && !registering\">\n<p>Pas encore inscrit ? <br/>Pas de problème !</p>\n<button class=\"btn btn-secondary\" (click)=\"registerForm()\">Créer un compte</button>\n</div>\n<form action=\"\" (ngSubmit)=\"onSubmitRegister()\" #f=\"ngForm\" *ngIf=\"!logged && registering\" >\n    <div class=\"form-group\">\n      <label for=\"account\">Account :</label>\n      <input type=\"text\" class=\"form-control\" id=\"account\" name=\"account\" [(ngModel)]=user.account required>\n    </div>\n    <div class=\"form-group\">\n        <label for=\"password\">Password :</label>\n        <input type=\"password\" class=\"form-control\" id=\"password\" name=\"password\" [(ngModel)]=user.password required>\n        <label for=\"passwordBis\">Repeat password :</label>\n        <input type=\"password\" class=\"form-control\" [ngStyle]=\"{'border-color':passwordBisColor}\" id=\"passwordBis\" name=\"passwordBis\" [(ngModel)]=passwordBis required>\n      </div>\n      <button type=\"submit\" class=\"btn btn-primary\">Register</button>\n  </form>\n<div *ngIf=\"logged\">\n  <h1>Vous êtes déja connecté</h1>\n</div>\n\n"

/***/ }),

/***/ "./src/app/auth/auth.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthComponent = /** @class */ (function () {
    function AuthComponent(requestService, router) {
        this.requestService = requestService;
        this.router = router;
        this.user = { account: "", password: "" };
        this.registering = false;
        this.passwordBis = "";
        this.passwordBisColor = "";
    }
    AuthComponent.prototype.onSubmit = function () {
        var _this = this;
        this.requestService.authentifie(this.user).subscribe(function (data) {
            if (data.logged) {
                _this.requestService.valide(data.nom, data.recettesTestees, data.token);
                _this.router.navigateByUrl("/recettes");
            }
        });
    };
    AuthComponent.prototype.registerForm = function () {
        this.registering = true;
    };
    AuthComponent.prototype.onSubmitRegister = function () {
        var _this = this;
        if (this.user.password == this.passwordBis) {
            this.requestService.ajoutUser(this.user).subscribe(function (data) {
                if (data.logged) {
                    _this.requestService.valide(_this.user.account, [], data.token);
                    _this.router.navigateByUrl("/recettes");
                }
            });
        }
        else {
            this.passwordBisColor = "red";
        }
    };
    AuthComponent.prototype.ngOnInit = function () {
        this.requestService.initialise();
        this.logged = this.requestService.loggedIn;
    };
    AuthComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-auth',
            template: __webpack_require__("./src/app/auth/auth.component.html"),
            styles: [__webpack_require__("./src/app/auth/auth.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], AuthComponent);
    return AuthComponent;
}());



/***/ }),

/***/ "./src/app/chat/chat.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/chat/chat.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <ul class=\"list-group\">\n    <li class=\"list-item\" *ngFor=\"let obj of messages\">{{obj.nom}} : {{obj.msg}}</li>\n  </ul>\n</div>\n<div>\n  <input [(ngModel)]=\"message\" placeholder=\"Votre message\">\n  <button (click)=\"sendMessage()\">Send</button>\n</div>"

/***/ }),

/***/ "./src/app/chat/chat.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_chat_service__ = __webpack_require__("./src/app/service/chat.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ChatComponent = /** @class */ (function () {
    function ChatComponent(chatService, requestService) {
        this.chatService = chatService;
        this.requestService = requestService;
        this.message = "";
        this.messages = [];
    }
    ChatComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.chatService.getMessages().subscribe(function (obj) {
            _this.messages.push(obj);
        });
    };
    ChatComponent.prototype.sendMessage = function () {
        this.chatService.sendMessage(this.message, this.requestService.user.nom);
    };
    ChatComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-chat',
            template: __webpack_require__("./src/app/chat/chat.component.html"),
            styles: [__webpack_require__("./src/app/chat/chat.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_chat_service__["a" /* ChatService */], __WEBPACK_IMPORTED_MODULE_2__service_request__["a" /* RequestService */]])
    ], ChatComponent);
    return ChatComponent;
}());



/***/ }),

/***/ "./src/app/commentaires/commentaires.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/commentaires/commentaires.component.html":
/***/ (function(module, exports) {

module.exports = "<ul class=\"list-group\">\n  <li class=\"list-group\" *ngFor=\"let commentaire of commentaires\">\n    <h3>{{commentaire.From}} a dit :</h3>\n    <p>{{commentaire.content}}</p>\n  </li>\n\n</ul>\n"

/***/ }),

/***/ "./src/app/commentaires/commentaires.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommentairesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CommentairesComponent = /** @class */ (function () {
    function CommentairesComponent(requestService) {
        this.requestService = requestService;
    }
    CommentairesComponent.prototype.addCommentaire = function (commentaire) {
        this.requestService.addCommentaire(this.recetteUrl, commentaire).subscribe(function (data) {
            console.log(data);
        });
    };
    CommentairesComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentairesComponent.prototype, "recetteUrl", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], CommentairesComponent.prototype, "commentaires", void 0);
    CommentairesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-commentaires',
            template: __webpack_require__("./src/app/commentaires/commentaires.component.html"),
            styles: [__webpack_require__("./src/app/commentaires/commentaires.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */]])
    ], CommentairesComponent);
    return CommentairesComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-sm bg-dark navbar-dark\">\n    <ul class=\"navbar-nav\">\n      <li class=\"nav-item\" routerLinkActive=\"active\">\n        <a class=\"nav-link\" routerLink=\"liste\">Liste des recettes</a>\n      </li>\n      <li class=\"nav-item\" routerLinkActive=\"active\">\n        <a class=\"nav-link\" routerLink=\"ajout\">Ajouter une recette</a>\n      </li>\n      <li class=\"nav-item\" routerLinkActive=\"active\">\n        <a class=\"nav-link\" routerLink=\"auth\">Authentification</a>\n      </li>\n    </ul>\n    <form class=\"form-inline\" style=\"position:absolute;right:50px;\" action=\"\">\n      <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\">\n      <button class=\"btn btn-success\" type=\"submit\">Search</button>\n    </form>\n  </nav>\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-header',
            template: __webpack_require__("./src/app/header/header.component.html"),
            styles: [__webpack_require__("./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/modifie-recette/modifie-recette.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/modifie-recette/modifie-recette.component.html":
/***/ (function(module, exports) {

module.exports = "<form action=\"\" (ngSubmit)=\"onSubmit()\" #f=\"ngForm\" >\n  <div class=\"form-group\">\n    <label for=\"nom\">Nom de la recette :</label>\n    <input type=\"text\" readonly class=\"form-control-plaintext\" id=\"nom\" name=\"nom\" [(ngModel)]=recette.nom>\n  </div>\n  <div class=\"form-group\">\n    <label for=\"difficulte\">Difficulté :</label>\n    <input type=\"number\" class=\"form-control\" id=\"difficulte\" name=\"difficulte\" [(ngModel)]=recette.difficulte>\n  </div>\n  <div class=\"form-group\">\n    <label for=\"origine\">Origine :</label>\n    <input type=\"text\" class=\"form-control\" id=\"origine\" name=\"origine\" [(ngModel)]=recette.origine>\n  </div>\n  <div class=\"form-group form-check\">\n    <label class=\"form-check-label\">\n      <input class=\"form-check-input\" type=\"checkbox\" name=\"vegan\" [(ngModel)]=recette.vegan> Vegan ?\n    </label>\n  </div>\n  <div class=\"form-group\" *ngFor=\"let i of array\">\n      <label for=\"ingredient\">Ingrédient :</label>\n      <input type=\"text\" class=\"form-control\" id=\"ingredient\" [name]=\"ingredient(i)\" [(ngModel)]=recette.ingredients[i].nom>\n      <label for=\"quantite\">Quantité :</label>\n      <input type=\"number\" class=\"form-control\" id=\"quantite\" [name]=\"quantite(i)\" [(ngModel)]=recette.ingredients[i].quantite>\n      <button class=\"btn btn-danger\" style=\"width:20px;height:30px;text-align:auto;font-size:15px;\" (click)=\"supprimeIngredient(i)\">X</button>\n    </div>\n   \n  <button class=\"btn btn-success\" (click)=\"addIngredient()\">+</button>\n  <div class=\"form-group\">\n      <label for=\"description\">Description de la recette :</label>\n      <textarea type=\"text\" style=\"height:300px;\" size=500 class=\"form-control\" id=\"description\" name=\"description\" [(ngModel)]=recette.description></textarea>\n    </div>\n  <button type=\"submit\" (click)=\"valide()\" class=\"btn btn-primary\">Submit</button>\n</form> "

/***/ }),

/***/ "./src/app/modifie-recette/modifie-recette.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModifieRecetteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__recette__ = __webpack_require__("./src/app/recette.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ModifieRecetteComponent = /** @class */ (function () {
    function ModifieRecetteComponent(requestService) {
        this.requestService = requestService;
        this.array = [0];
        this.validated = false;
    }
    ModifieRecetteComponent.prototype.addArray = function () {
        this.array.push(this.array[this.array.length - 1] + 1);
    };
    ModifieRecetteComponent.prototype.addIngredient = function () {
        this.addArray();
        this.recette.addIngredient("", 0);
    };
    ModifieRecetteComponent.prototype.ingredient = function (i) {
        return ("ingredient" + i);
    };
    ModifieRecetteComponent.prototype.quantite = function (i) {
        return ("quantite" + i);
    };
    ModifieRecetteComponent.prototype.supprimeIngredient = function (i) {
        this.recette.removeIngredient(i);
        this.array.pop();
    };
    ModifieRecetteComponent.prototype.valide = function () {
        this.validated = true;
    };
    ModifieRecetteComponent.prototype.onSubmit = function () {
        if (this.validated) {
            return this.requestService.modifieRecette(this.recette.url, this.recette).subscribe(function (data) {
            });
        }
    };
    ModifieRecetteComponent.prototype.ngOnInit = function () {
        for (var _i = 0, _a = this.recette.ingredients; _i < _a.length; _i++) {
            var ingredient = _a[_i];
            this.addArray();
        }
        this.array.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2__recette__["a" /* Recette */])
    ], ModifieRecetteComponent.prototype, "recette", void 0);
    ModifieRecetteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-modifie-recette',
            template: __webpack_require__("./src/app/modifie-recette/modifie-recette.component.html"),
            styles: [__webpack_require__("./src/app/modifie-recette/modifie-recette.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */]])
    ], ModifieRecetteComponent);
    return ModifieRecetteComponent;
}());



/***/ }),

/***/ "./src/app/recette-min/recette-min.component.css":
/***/ (function(module, exports) {

module.exports = "button{\r\n    width:-webkit-fit-content;\r\n    width:-moz-fit-content;\r\n    width:fit-content;\r\n}"

/***/ }),

/***/ "./src/app/recette-min/recette-min.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>{{recette.nom}}</h3>\n<p *ngIf=\"recette.vegan\" style=\"color:green\">Vegan</p>\n<p *ngIf=\"!recette.vegan\" style=\"color:red\">Pas vegan</p>\n<a class=\"btn btn-success\" [routerLink]=\"genereLien(recette.nom)\">Détails</a>\n<button class=\"btn btn-danger\" style=\"width:20px;height:30px;text-align:auto;font-size:15px;\" routerLink=\"/\" (click)=\"supprime()\">X</button>\n"

/***/ }),

/***/ "./src/app/recette-min/recette-min.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecetteMinComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__recette__ = __webpack_require__("./src/app/recette.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RecetteMinComponent = /** @class */ (function () {
    function RecetteMinComponent(requestService) {
        this.requestService = requestService;
    }
    RecetteMinComponent.prototype.supprime = function () {
        this.requestService.supprimeRecette(this.recette.nom).subscribe(function (data) {
        });
    };
    RecetteMinComponent.prototype.createRecette = function (json) {
        return new __WEBPACK_IMPORTED_MODULE_1__recette__["a" /* Recette */](json.nom, json.difficulte, json.origine, json.ingredients, json.vegan, json.description, []);
    };
    RecetteMinComponent.prototype.genereLien = function (nom) {
        var s = "";
        for (var _i = 0, _a = nom.toLowerCase(); _i < _a.length; _i++) {
            var lettre = _a[_i];
            s += lettre;
            if (lettre == "é" || lettre == "è" || lettre == "ê") {
                s = s.slice(0, -1);
                s += "e";
            }
            if (lettre == "à" || lettre == "â") {
                s = s.slice(0, -1);
                s += "a";
            }
            if (lettre == "ô") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "ù") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "î") {
                s = s.slice(0, -1);
                s += "i";
            }
            if (lettre == " ") {
                s = s.slice(0, -1);
                s += "-";
            }
        }
        return s;
    };
    RecetteMinComponent.prototype.ngOnInit = function () {
        this.recette = this.createRecette(this.recetteJson);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object)
    ], RecetteMinComponent.prototype, "recetteJson", void 0);
    RecetteMinComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-recette-min',
            template: __webpack_require__("./src/app/recette-min/recette-min.component.html"),
            styles: [__webpack_require__("./src/app/recette-min/recette-min.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__service_request__["a" /* RequestService */]])
    ], RecetteMinComponent);
    return RecetteMinComponent;
}());



/***/ }),

/***/ "./src/app/recette.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Recette; });
var Ingredient = /** @class */ (function () {
    function Ingredient(n, q) {
        this.nom = n;
        this.quantite = q;
    }
    Ingredient.prototype.changeQuantite = function (nouvelleQuantite) {
        this.quantite = nouvelleQuantite;
    };
    return Ingredient;
}());
var Recette = /** @class */ (function () {
    function Recette(n, di, o, i, v, de, coms) {
        this.nom = n;
        this.url = this.genereLien(n);
        this.difficulte = di;
        this.origine = o;
        this.ingredients = [];
        this.vegan = v;
        this.description = de;
        this.commentaires = coms;
        for (var _i = 0, i_1 = i; _i < i_1.length; _i++) {
            var ingredient = i_1[_i];
            this.ingredients.push(new Ingredient(ingredient.nom, ingredient.quantite));
        }
    }
    Recette.prototype.genereLien = function (nom) {
        var s = "";
        var accent = false;
        console.log(nom.toLowerCase());
        for (var _i = 0, _a = nom.toLowerCase(); _i < _a.length; _i++) {
            var lettre = _a[_i];
            s += lettre;
            if (lettre == "é" || lettre == "è" || lettre == "ê") {
                s = s.slice(0, -1);
                s += "e";
            }
            if (lettre == "à" || lettre == "â") {
                s = s.slice(0, -1);
                s += "a";
            }
            if (lettre == "ô") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "ù") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "î") {
                s = s.slice(0, -1);
                s += "i";
            }
            if (lettre == " ") {
                s = s.slice(0, -1);
                s += "-";
            }
        }
        return s;
    };
    Recette.prototype.changeQuantites = function (nouvellesQuantites) {
        for (var _i = 0, nouvellesQuantites_1 = nouvellesQuantites; _i < nouvellesQuantites_1.length; _i++) {
            var modification = nouvellesQuantites_1[_i];
            for (var _a = 0, _b = this.ingredients; _a < _b.length; _a++) {
                var ingredient = _b[_a];
                if (modification.nomIngredient == ingredient.nom) {
                    ingredient.changeQuantite(modification.nouvelleQuantite);
                }
            }
        }
    };
    Recette.prototype.addIngredient = function (ingredientNom, quantite) {
        var newIngredient = new Ingredient(ingredientNom, quantite);
        this.ingredients.push(newIngredient);
    };
    Recette.prototype.removeIngredient = function (i) {
        this.ingredients.splice(i, 1);
        console.log(this.ingredients);
    };
    Recette.prototype.displayIngredients = function () {
        var liste = [];
        for (var _i = 0, _a = this.ingredients; _i < _a.length; _i++) {
            var ingredient = _a[_i];
            liste.push({ nom: ingredient.nom, quantite: ingredient.quantite });
        }
        return liste;
    };
    return Recette;
}());



/***/ }),

/***/ "./src/app/recette/recette.component.css":
/***/ (function(module, exports) {

module.exports = ".ajout_img{\r\n    width:300px;\r\n    height:300px;\r\n    position:absolute;\r\n    top:100px;\r\n    right:500px;\r\n    border-width:10px;\r\n    border-color:dimgrey;\r\n    text-align: center;\r\n    font-size: 20px;\r\n}\r\n\r\n.form-group{\r\n    border-style:dashed;\r\n}\r\n\r\n.affiche_text{\r\n    background-color: rgba(0, 0, 0, 0.6);\r\n}"

/***/ }),

/***/ "./src/app/recette/recette.component.html":
/***/ (function(module, exports) {

module.exports = "<h3>{{recette.nom}}</h3>\n<h3>Difficulte : {{recette.difficulte}}</h3>\n<p *ngIf=\"!isLiked\"><fa name=\"heart\" style=\"color:red;font-size:50px;\" (click)=\"like()\"></fa></p>\n<button class=\"btn btn-success\" (click)=\"switchSavoirPlus()\">Voir la recette</button>\n<div *ngIf=\"savoirPlus\">\n<h4>Ingredients :</h4>\n<ul class=\"affiche_text\">\n  <li *ngFor=\"let ingredient of recette.displayIngredients()\">{{ingredient.nom}} : Quantités : {{ingredient.quantite}} g</li>\n</ul>\n<h4 class=\"affiche_text\">Recette :</h4>\n<p class=\"affiche_text\">{{recette.description}}</p>\n</div>\n<button class=\"btn btn-secondary\" (click)=\"modifier()\" *ngIf=\"savoirPlus\">Modifier</button>\n<div *ngIf=\"!image\" class=\"form-group ajout_img\">\nAjouter une image\n<input type=\"file\" id=\"image\" #image (change)=\"upload(image)\">\n</div>\n\n<img *ngIf=\"image\" class=\"ajout_img\" [src]=\"imageSafe\">\n\n<div>\n  <div>\n<app-modifie-recette *ngIf=\"modifie\" [recette]=\"recette\"></app-modifie-recette>\n</div>\n<div>\n<app-commentaires [recetteUrl]=\"url\" [commentaires]=\"recette.commentaires\"></app-commentaires>\n<button class=\"btn btn-secondary\" (click)=\"commenter()\">Commenter</button>\n<form *ngIf=\"commente\" (ngSubmit)=\"onSubmit()\">\n<input type=\"text\" [(ngModel)]=\"commentaire\" name=\"commentaire\" placeholder=\"Votre commentaire\">\n<button type=\"submit\" (click)=\"valide()\">Envoyer</button>\n</form>\n</div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/recette/recette.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecetteComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__recette__ = __webpack_require__("./src/app/recette.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__service_request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RecetteComponent = /** @class */ (function () {
    function RecetteComponent(sanitizer, router, requestService, routeur) {
        var _this = this;
        this.sanitizer = sanitizer;
        this.router = router;
        this.requestService = requestService;
        this.routeur = routeur;
        this.savoirPlus = false;
        this.modifie = false;
        this.isLiked = false;
        this.image = false;
        this.commente = false;
        this.okSubmit = false;
        this.router.params.subscribe(function (params) { return _this.url = params.recetteUrl; });
    }
    RecetteComponent.prototype.upload = function (file) {
        var _this = this;
        this.imageToUpload = file.files[0];
        this.requestService.uploadImg(this.url, file.files[0]).subscribe(function (data) {
            _this.routeur.navigateByUrl("recettes/");
        });
    };
    RecetteComponent.prototype.like = function () {
        var _this = this;
        this.requestService.like(this.recette.nom).subscribe(function (data) {
            _this.isLiked = true;
        });
    };
    RecetteComponent.prototype.getImage = function () {
        var _this = this;
        this.requestService.getImage(this.url).subscribe(function (img) {
            if (img) {
                _this.image = true;
                var imageToDisplay = img.image;
                _this.imageSafe = _this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + imageToDisplay);
            }
        });
    };
    RecetteComponent.prototype.modifier = function () {
        this.modifie = true;
    };
    RecetteComponent.prototype.switchSavoirPlus = function () {
        this.savoirPlus = !this.savoirPlus;
    };
    RecetteComponent.prototype.createRecette = function (json) {
        return new __WEBPACK_IMPORTED_MODULE_1__recette__["a" /* Recette */](json.nom, json.difficulte, json.origine, json.ingredients, json.vegan, json.description, json.commentaires);
    };
    RecetteComponent.prototype.getRecette = function () {
        var _this = this;
        return this.requestService.getRecette(this.url).subscribe(function (data) {
            _this.recette = _this.createRecette(data);
            for (var _i = 0, _a = _this.requestService.user.recettesTestees; _i < _a.length; _i++) {
                var recetteT = _a[_i];
                if (_this.recette.nom == recetteT) {
                    _this.isLiked = true;
                }
            }
        });
    };
    RecetteComponent.prototype.commenter = function () {
        this.commente = true;
    };
    RecetteComponent.prototype.valide = function () {
        this.okSubmit = true;
    };
    RecetteComponent.prototype.onSubmit = function () {
        if (this.okSubmit) {
            this.requestService.addCommentaire(this.url, this.commentaire).subscribe(function (data) {
            });
        }
    };
    RecetteComponent.prototype.ngOnInit = function () {
        this.routeur.routeReuseStrategy.shouldReuseRoute = function () { return false; };
        this.getRecette();
        this.getImage();
    };
    RecetteComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-recette',
            template: __webpack_require__("./src/app/recette/recette.component.html"),
            styles: [__webpack_require__("./src/app/recette/recette.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_platform_browser__["b" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_3__service_request__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], RecetteComponent);
    return RecetteComponent;
}());



/***/ }),

/***/ "./src/app/recettes-affichage/recettes-affichage.component.css":
/***/ (function(module, exports) {

module.exports = ".rangee{\r\n    display:-webkit-box;\r\n    display:-ms-flexbox;\r\n    display:flex;\r\n\r\n}\r\n\r\n.liste{\r\n    width:30%;\r\n    display:-webkit-box;\r\n    display:-ms-flexbox;\r\n    display:flex;\r\n    -ms-flex-wrap:wrap;\r\n        flex-wrap:wrap;\r\n}\r\n\r\n.element{\r\n    width:80%;\r\n}\r\n"

/***/ }),

/***/ "./src/app/recettes-affichage/recettes-affichage.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"rangee\">\n        <ul class=\"liste\">\n            <app-recette-min class=\"element\" *ngFor=\"let recette of recettes\" [recetteJson]=\"recette\"></app-recette-min>\n        </ul>\n        <div> \n            <h2>Vous avez aimé</h2>\n            <ul>\n                <li *ngFor=\"let recetteTestee of recettesTestees\">{{recetteTestee}}</li>\n            </ul>\n        </div>\n    </div>\n</div>\n"

/***/ }),

/***/ "./src/app/recettes-affichage/recettes-affichage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecettesAffichageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecettesAffichageComponent = /** @class */ (function () {
    function RecettesAffichageComponent(requestService) {
        this.requestService = requestService;
        this.nom = "";
        this.recettesTestees = [];
    }
    RecettesAffichageComponent.prototype.getRecettes = function () {
        var _this = this;
        this.requestService.getRecettes().subscribe(function (data) {
            _this.recettes = data;
        });
    };
    RecettesAffichageComponent.prototype.ngOnInit = function () {
        this.nom = this.requestService.user.nom;
        this.recettesTestees = this.requestService.user.recettesTestees;
        this.getRecettes();
    };
    RecettesAffichageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-recettes-affichage',
            template: __webpack_require__("./src/app/recettes-affichage/recettes-affichage.component.html"),
            styles: [__webpack_require__("./src/app/recettes-affichage/recettes-affichage.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_request__["a" /* RequestService */]])
    ], RecettesAffichageComponent);
    return RecettesAffichageComponent;
}());



/***/ }),

/***/ "./src/app/recherche/recherche.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/recherche/recherche.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"form-inline\" style=\"position:absolute;right:50px;top:10px;\" action=\"\">\n  <input class=\"form-control mr-sm-2\" type=\"text\" (click)=\"onClick()\" placeholder=\"Chercher une recette\" #box (keyup)=\"onKey(box.value)\">\n</form>\n<div style=\"position:absolute;right:100px;top:55px;\">\n<ul class=\"list-group\">\n  <li *ngFor=\"let resultat of resultats[0]\" class=\"list-group-item\" (click)=\"goTo(resultat.url)\">{{resultat.nom}}\n      <img style=\"width:50px;\" [src]=\"resultats[1][resultat.url]\">\n  </li>\n</ul>\n</div>"

/***/ }),

/***/ "./src/app/recherche/recherche.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RechercheComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__service_recherche__ = __webpack_require__("./src/app/service/recherche.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RechercheComponent = /** @class */ (function () {
    function RechercheComponent(rechercheService, router) {
        this.rechercheService = rechercheService;
        this.router = router;
        this.resultats = [];
    }
    RechercheComponent.prototype.onClick = function () {
        this.rechercheService.onClick();
    };
    RechercheComponent.prototype.onKey = function (chaine) {
        if (chaine == "") {
            this.resultats = [];
        }
        else {
            this.resultats = this.rechercheService.chercheRecettes(chaine);
        }
    };
    RechercheComponent.prototype.goTo = function (url) {
        this.router.navigateByUrl("recettes/" + url);
        this.resultats = [];
    };
    RechercheComponent.prototype.ngOnInit = function () {
    };
    RechercheComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-recherche',
            template: __webpack_require__("./src/app/recherche/recherche.component.html"),
            styles: [__webpack_require__("./src/app/recherche/recherche.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__service_recherche__["a" /* RechercheService */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], RechercheComponent);
    return RechercheComponent;
}());



/***/ }),

/***/ "./src/app/service/auth-guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(requestService, router) {
        this.requestService = requestService;
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (this.requestService.loggedIn) {
            return true;
        }
        else {
            this.router.navigate(["/auth"]);
        }
    };
    AuthGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__request__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* Router */]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/service/chat.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__ = __webpack_require__("./node_modules/rxjs/_esm5/Observable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client__ = __webpack_require__("./node_modules/socket.io-client/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_socket_io_client__);


var ChatService = /** @class */ (function () {
    function ChatService() {
        this.url = 'http://localhost:8080';
        this.socket = __WEBPACK_IMPORTED_MODULE_1_socket_io_client__(this.url);
    }
    ChatService.prototype.ngOnInit = function () {
    };
    ChatService.prototype.sendMessage = function (message, userNom) {
        this.socket.emit('nouveau-message', { msg: message, nom: userNom });
    };
    ChatService.prototype.getMessages = function () {
        var _this = this;
        return __WEBPACK_IMPORTED_MODULE_0_rxjs_Observable__["a" /* Observable */].create(function (observer) {
            _this.socket.on('nouveau-message', function (obj) {
                observer.next(obj);
            });
        });
    };
    return ChatService;
}());



/***/ }),

/***/ "./src/app/service/recherche.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RechercheService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__request__ = __webpack_require__("./src/app/service/request.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/esm5/platform-browser.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RechercheService = /** @class */ (function () {
    function RechercheService(requestService, sanitizer) {
        this.requestService = requestService;
        this.sanitizer = sanitizer;
    }
    RechercheService.prototype.onClick = function () {
        var _this = this;
        this.requestService.getRecettes().subscribe(function (data) {
            _this.recettes = data;
        });
    };
    RechercheService.prototype.chercheRecettes = function (recetteMot) {
        var _this = this;
        var recetteUrl = this.requestService.genereLien(recetteMot);
        var recettesTrouvees = [];
        var imagesTrouvees = {};
        var regexp = new RegExp(recetteUrl);
        var _loop_1 = function (recette) {
            if (regexp.test("/" + recette.url)) {
                recettesTrouvees.push(recette);
                this_1.requestService.getImage(recette.url).subscribe(function (img) {
                    if (img) {
                        imagesTrouvees[recette.url] = _this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + img.image);
                    }
                });
            }
        };
        var this_1 = this;
        for (var _i = 0, _a = this.recettes; _i < _a.length; _i++) {
            var recette = _a[_i];
            _loop_1(recette);
        }
        return [recettesTrouvees, imagesTrouvees];
    };
    RechercheService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__request__["a" /* RequestService */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["b" /* DomSanitizer */]])
    ], RechercheService);
    return RechercheService;
}());



/***/ }),

/***/ "./src/app/service/request.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var httpOptions = {
    headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({
        "Content-Type": "application/json"
    })
};
function buildOptionsGet(tok) {
    var httpOptionsGet = {
        headers: new __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["d" /* HttpHeaders */]({
            "Content-Type": "application/json"
        })
    };
    return httpOptionsGet;
}
var RequestService = /** @class */ (function () {
    function RequestService(httpClient, router) {
        this.httpClient = httpClient;
        this.router = router;
        this.token = "";
        this.loggedIn = false;
        this.user = { nom: "", recettesTestees: [], recettesRecommandees: [] };
        this.serverUrl = "http://localhost:8080";
    }
    RequestService.prototype.genereLien = function (nom) {
        var s = "/";
        for (var _i = 0, _a = nom.toLowerCase(); _i < _a.length; _i++) {
            var lettre = _a[_i];
            s += lettre;
            if (lettre == "é" || lettre == "è" || lettre == "ê") {
                s = s.slice(0, -1);
                s += "e";
            }
            if (lettre == "à" || lettre == "â") {
                s = s.slice(0, -1);
                s += "a";
            }
            if (lettre == "ô") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "ù") {
                s = s.slice(0, -1);
                s += "o";
            }
            if (lettre == "î") {
                s = s.slice(0, -1);
                s += "i";
            }
            if (lettre == " ") {
                s = s.slice(0, -1);
                s += "-";
            }
        }
        return s;
    };
    RequestService.prototype.initialise = function () {
        var _this = this;
        this.httpClient.get(this.serverUrl + "/isLogged").subscribe(function (data) {
            _this.loggedIn = data.logged;
            _this.user.nom = data.user.nom;
            _this.user.recettesTestees = data.user.recettesTestees;
        });
    };
    RequestService.prototype.getRecettes = function () {
        console.log(this.token);
        return this.httpClient.get(this.serverUrl + "/recettes");
    };
    RequestService.prototype.getRecette = function (recetteUrl) {
        return this.httpClient.get(this.serverUrl + "/recettes" + "/" + recetteUrl);
    };
    RequestService.prototype.supprimeRecette = function (recetteNom) {
        var recetteUrl = this.genereLien(recetteNom);
        return this.httpClient.get(this.serverUrl + "/supprime" + recetteUrl);
    };
    RequestService.prototype.ajouteRecette = function (recette) {
        this.router.navigateByUrl("/recettes");
        return this.httpClient.post(this.serverUrl + "/ajout", JSON.stringify(recette), httpOptions);
    };
    RequestService.prototype.like = function (recetteNom) {
        this.user.recettesTestees.push(recetteNom);
        return this.httpClient.post(this.serverUrl + "/testeRecette/" + this.user.nom, JSON.stringify({ recetteNom: recetteNom }), buildOptionsGet(this.token));
    };
    RequestService.prototype.modifieRecette = function (recetteUrl, recette) {
        this.router.navigateByUrl("/recettes");
        return this.httpClient.post(this.serverUrl + "/modifie/" + recetteUrl, JSON.stringify(recette), httpOptions);
    };
    RequestService.prototype.uploadImg = function (recetteUrl, img) {
        var formData = new FormData();
        formData.append("image", img);
        console.log(img);
        return this.httpClient.post(this.serverUrl + "/addImage/" + recetteUrl, formData);
    };
    RequestService.prototype.getImage = function (recetteUrl) {
        return this.httpClient.get(this.serverUrl + "/images/" + recetteUrl);
    };
    RequestService.prototype.addCommentaire = function (recetteUrl, commentaire) {
        return this.httpClient.post(this.serverUrl + "/commente/" + recetteUrl, JSON.stringify({ commentaire: { From: this.user.nom, content: commentaire } }), httpOptions);
    };
    RequestService.prototype.ajoutUser = function (user) {
        return this.httpClient.post(this.serverUrl + "/addUser", JSON.stringify({ account: user.account, password: user.password }), httpOptions);
    };
    RequestService.prototype.authentifie = function (user) {
        return this.httpClient.post(this.serverUrl + "/auth", JSON.stringify({ account: user.account, password: user.password }), httpOptions);
    };
    RequestService.prototype.valide = function (nom, recettesTestees, tok) {
        this.token = tok;
        this.user.nom = nom;
        this.user.recettesTestees = recettesTestees;
        this.loggedIn = true;
    };
    RequestService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_common_http__["b" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]])
    ], RequestService);
    return RequestService;
}());



/***/ }),

/***/ "./src/app/service/token.interceptor.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TokenInterceptor; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__request__ = __webpack_require__("./src/app/service/request.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(requestService) {
        this.requestService = requestService;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: "Token " + this.requestService.token
            }
        });
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__request__["a" /* RequestService */]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/test-component/test-component.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/test-component/test-component.component.html":
/***/ (function(module, exports) {

module.exports = "<p>\nOn m'appelle {{nom}}\n</p>\n<button [class]=\"couleur\" (click)=\"clique()\">{{approbationText}}</button>\n<input [(ngModel)]=\"nom\">\n"

/***/ }),

/***/ "./src/app/test-component/test-component.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TestComponentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TestComponentComponent = /** @class */ (function () {
    function TestComponentComponent() {
        this.nom = "l'OVNI !";
        this.couleur = "btn btn-success";
    }
    TestComponentComponent.prototype.clique = function () {
        if (this.couleur == "btn btn-success") {
            this.couleur = "btn btn-danger";
        }
        else {
            this.couleur = "btn btn-success";
        }
    };
    TestComponentComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], TestComponentComponent.prototype, "approbationText", void 0);
    TestComponentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'app-test-component',
            template: __webpack_require__("./src/app/test-component/test-component.component.html"),
            styles: [__webpack_require__("./src/app/test-component/test-component.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TestComponentComponent);
    return TestComponentComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ }),

/***/ 1:
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map