import { Component, OnInit } from '@angular/core';
import { RequestService } from "../service/request";
import { Router } from '@angular/router';



@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  user={account:"",password:""};
  logged:boolean;
  registering=false;
  passwordBis="";
  passwordBisColor="";
  

  onSubmit(){
    this.requestService.authentifie(this.user).subscribe((data:any) => {
      if(data.logged){
        this.requestService.valide(data.nom,data.recettesTestees,data.token);
        this.router.navigateByUrl("/recettes");
      }
    });
  }

  registerForm(){
    this.registering=true;
  }

  onSubmitRegister(){
    if(this.user.password==this.passwordBis){
      this.requestService.ajoutUser(this.user).subscribe((data:any) => {
        if(data.logged){
          this.requestService.valide(this.user.account,[],data.token);
          this.router.navigateByUrl("/recettes");
        }
      });
    }else{
      this.passwordBisColor="red";
    }
  }



  constructor(private requestService:RequestService, private router:Router) { }

  ngOnInit() {
    this.requestService.initialise();
    this.logged=this.requestService.loggedIn;
  }

}
