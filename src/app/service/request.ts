import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

let httpOptions = {
    headers: new HttpHeaders({
    "Content-Type":"application/json"
    })
};

function buildOptionsGet(tok){
    let httpOptionsGet = {
        headers: new HttpHeaders({
            "Content-Type":"application/json"
        })
    };
    return httpOptionsGet;
}



@Injectable()

export class RequestService {

    constructor(private httpClient: HttpClient,private router:Router) {
    }
    token="";

    loggedIn=false;
    user={nom:"",recettesTestees:[], recettesRecommandees:[]};
    serverUrl="http://localhost:8080";

    genereLien(nom){
    let s="/";
    for(let lettre of nom.toLowerCase()){
        s+=lettre;
        if(lettre=="é" || lettre=="è" || lettre=="ê"){
            s=s.slice(0,-1);
            s+="e";
        }
        if(lettre=="à" || lettre=="â"){
            s=s.slice(0,-1);
            s+="a";
        }
        if(lettre=="ô"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="ù"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="î"){
            s=s.slice(0,-1);
            s+="i";
        }
        if(lettre==" "){
            s=s.slice(0,-1);
            s+="-";
        }
    }
    return s;
    }


    
    initialise() {
        this.httpClient.get(this.serverUrl+"/isLogged").subscribe((data:any) => {
            this.loggedIn=data.logged;
            this.user.nom=data.user.nom;
            this.user.recettesTestees=data.user.recettesTestees;
        });
    }

    getRecettes(){
        console.log(this.token);
        return this.httpClient.get(this.serverUrl+"/recettes");
    }

    getRecette(recetteUrl){
        return this.httpClient.get(this.serverUrl+"/recettes"+"/"+recetteUrl);
    }

    supprimeRecette(recetteNom){
        let recetteUrl=this.genereLien(recetteNom);
        return this.httpClient.get(this.serverUrl+"/supprime"+recetteUrl)
    }

    ajouteRecette(recette){
        this.router.navigateByUrl("/recettes");
        return this.httpClient.post(this.serverUrl+"/ajout",JSON.stringify(recette), httpOptions);
    }

    like(recetteNom){
        this.user.recettesTestees.push(recetteNom);
        return this.httpClient.post(this.serverUrl+"/testeRecette/"+this.user.nom,JSON.stringify({recetteNom:recetteNom}), buildOptionsGet(this.token));
    }

    modifieRecette(recetteUrl,recette){
        this.router.navigateByUrl("/recettes");
        return this.httpClient.post(this.serverUrl+"/modifie/"+recetteUrl,JSON.stringify(recette), httpOptions);
    }

    uploadImg(recetteUrl,img){
        const formData = new FormData();
        formData.append("image",img);
        console.log(img);
        return this.httpClient.post(this.serverUrl+"/addImage/"+recetteUrl,formData);
    }

    getImage(recetteUrl){
        return this.httpClient.get(this.serverUrl+"/images/"+recetteUrl);
    }

    addCommentaire(recetteUrl, commentaire){
        return this.httpClient.post(this.serverUrl+"/commente/"+recetteUrl,JSON.stringify({commentaire:{From:this.user.nom,content:commentaire}}), httpOptions);
    }


    ajoutUser(user){
        return this.httpClient.post(this.serverUrl+"/addUser",JSON.stringify({account:user.account,password:user.password}), httpOptions);
    }

    authentifie(user){
        return this.httpClient.post(this.serverUrl+"/auth",JSON.stringify({account:user.account,password:user.password}), httpOptions);
    }

    valide(nom,recettesTestees, tok){
        this.token=tok;
        this.user.nom=nom;
        this.user.recettesTestees=recettesTestees;
        this.loggedIn=true;
    }

}