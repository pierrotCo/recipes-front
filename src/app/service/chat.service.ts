import { RequestService } from './request';
import { Observable } from "rxjs/Observable";
import * as io from 'socket.io-client';

export class ChatService {

    private url = 'http://localhost:8080';
    socket:any;

    constructor() {
        this.socket = io(this.url);
    }

    ngOnInit(){
    }

    sendMessage(message,userNom) {
        this.socket.emit('nouveau-message', {msg: message, nom:userNom});
    }

    getMessages(){
        return Observable.create((observer) => {
            this.socket.on('nouveau-message', (obj) => {
                observer.next(obj);
            });
        });
}
}