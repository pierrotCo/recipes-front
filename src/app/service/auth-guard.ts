import { ActivatedRouteSnapshot, CanActivate,Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RequestService } from './request';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private requestService: RequestService, private router:Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if(this.requestService.loggedIn){
            return true;
        }else{
            this.router.navigate(["/auth"]);
        }

  }
}