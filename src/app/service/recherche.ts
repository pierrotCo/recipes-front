import { Injectable } from '@angular/core';
import { RequestService } from "./request";
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()

export class RechercheService {

    constructor(private requestService:RequestService, private sanitizer: DomSanitizer){
    }
    recettes:any[];

    onClick(){
        this.requestService.getRecettes().subscribe((data:any[]) => {
            this.recettes=data;
        });
    }


    chercheRecettes(recetteMot){
        let recetteUrl=this.requestService.genereLien(recetteMot);
        let recettesTrouvees=[];
        let imagesTrouvees={};
        let regexp=new RegExp(recetteUrl);
        for (let recette of this.recettes){
            if(regexp.test("/"+recette.url)){
                recettesTrouvees.push(recette);
                this.requestService.getImage(recette.url).subscribe( (img: any) => {
                    if(img){
                        imagesTrouvees[recette.url]=this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+img.image);
                    }
                });
            }
        }
    return [recettesTrouvees,imagesTrouvees];
    }
}