import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RequestService } from "./service/request";
import { RechercheService } from "./service/recherche";
import { ChatService } from './service/chat.service';
import { TokenInterceptor } from "./service/token.interceptor";
import { AngularFontAwesomeModule } from 'angular-font-awesome';


import { AppComponent } from './app.component';
import { TestComponentComponent } from './test-component/test-component.component';
import { RecetteComponent } from './recette/recette.component';
import { HeaderComponent } from './header/header.component';
import { RecettesAffichageComponent } from './recettes-affichage/recettes-affichage.component';
import { RecetteMinComponent } from './recette-min/recette-min.component';
import { AuthComponent } from './auth/auth.component';
import { AuthGuard } from "./service/auth-guard";
import { AjoutRecetteComponent } from './ajout-recette/ajout-recette.component';

import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModifieRecetteComponent } from './modifie-recette/modifie-recette.component';
import { RechercheComponent } from './recherche/recherche.component';
import { CommentairesComponent } from './commentaires/commentaires.component';
import { ChatComponent } from './chat/chat.component';



const appRoutes: Routes = [
  { path: 'recettes', canActivate: [AuthGuard] , component: RecettesAffichageComponent, runGuardsAndResolvers:"always" },
  { path: "recettes/:recetteUrl", canActivate: [AuthGuard] , component: RecetteComponent,  runGuardsAndResolvers:"always" },
  { path: "ajout",canActivate: [AuthGuard], component: AjoutRecetteComponent },
  { path: 'auth', component: AuthComponent },
  { path: "chat", canActivate: [AuthGuard], component: ChatComponent },
  { path: '',canActivate: [AuthGuard], component: RecettesAffichageComponent }
];



@NgModule({
  declarations: [
    AppComponent,
    TestComponentComponent,
    RecetteComponent,
    HeaderComponent,
    RecettesAffichageComponent,
    RecetteMinComponent,
    AuthComponent,
    AjoutRecetteComponent,
    ModifieRecetteComponent,
    RechercheComponent,
    CommentairesComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes,{onSameUrlNavigation: "reload"}),
    HttpClientModule,
    AngularFontAwesomeModule
  ],
  providers: [
    RequestService,
    AuthGuard,
    RechercheService,
    ChatService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
