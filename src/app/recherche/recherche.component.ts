import { Component, OnInit } from '@angular/core';
import { RechercheService } from "../service/recherche";
import { Router } from '@angular/router';

@Component({
  selector: 'app-recherche',
  templateUrl: './recherche.component.html',
  styleUrls: ['./recherche.component.css']
})
export class RechercheComponent implements OnInit {
  resultats=[];

  onClick(){
    this.rechercheService.onClick();
  }

  onKey(chaine){
    if(chaine==""){
      this.resultats=[];
    }else{
      this.resultats=this.rechercheService.chercheRecettes(chaine);
    }
  }

  

  goTo(url){
    this.router.navigateByUrl("recettes/"+url);
    this.resultats=[];
  }

  constructor(private rechercheService:RechercheService, private router:Router) { }

  ngOnInit() {
  }

}
