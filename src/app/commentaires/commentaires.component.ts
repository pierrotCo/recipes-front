import { Component, OnInit, Input } from '@angular/core';
import { RequestService } from '../service/request';

@Component({
  selector: 'app-commentaires',
  templateUrl: './commentaires.component.html',
  styleUrls: ['./commentaires.component.css']
})
export class CommentairesComponent implements OnInit {

  @Input() recetteUrl;
  @Input() commentaires;


  addCommentaire(commentaire){
    this.requestService.addCommentaire(this.recetteUrl, commentaire).subscribe( (data:any) => {
      console.log(data);
    });
  }


  constructor(private requestService: RequestService) { }

  ngOnInit() {
  }

}
