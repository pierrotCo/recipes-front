import { Component, OnInit,Input } from '@angular/core';
import {Recette} from "../recette";
import {ActivatedRoute, Router} from "@angular/router";
import { RequestService } from "../service/request";
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-recette',
  templateUrl: './recette.component.html',
  styleUrls: ['./recette.component.css']
})
export class RecetteComponent implements OnInit {
  url:string;
  recette:Recette;
  savoirPlus=false;
  modifie=false;
  isLiked=false;
  imageToUpload:File;
  imageSafe:any;
  image=false;
  commente=false;
  commentaire:string;
  okSubmit=false;

  upload(file){
    this.imageToUpload=file.files[0];
    this.requestService.uploadImg(this.url,file.files[0]).subscribe((data:any) => {
      this.routeur.navigateByUrl("recettes/");
    });
  }

  like(){
    this.requestService.like(this.recette.nom).subscribe( (data:any) => {
      this.isLiked=true;
    });
  }

  getImage(){
    this.requestService.getImage(this.url).subscribe( (img: any) => {
      if(img){
        this.image=true;
        let imageToDisplay=img.image;
        this.imageSafe = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,'+imageToDisplay);
      }
    });
  }

  modifier(){
    this.modifie=true;
  }
  switchSavoirPlus(){
    this.savoirPlus=!this.savoirPlus;
  }
  createRecette(json){
    return new Recette(json.nom,json.difficulte,json.origine,json.ingredients,json.vegan,json.description,json.commentaires);
  }

  getRecette(){
    return this.requestService.getRecette(this.url).subscribe((data:any) => {
      this.recette=this.createRecette(data);
      for(let recetteT of this.requestService.user.recettesTestees){
        if(this.recette.nom==recetteT){
          this.isLiked=true;
        }
      }
    });
  }

  commenter(){
    this.commente=true;
  }

  valide(){
    this.okSubmit=true;
  }

  onSubmit(){
    if(this.okSubmit){
      this.requestService.addCommentaire(this.url,this.commentaire).subscribe( (data:any) => {
      });
    }
  }

  constructor(private sanitizer: DomSanitizer, private router:ActivatedRoute, private requestService: RequestService,private routeur: Router) { 
    this.router.params.subscribe( params => this.url = params.recetteUrl );
  }

  ngOnInit() {
    this.routeur.routeReuseStrategy.shouldReuseRoute = () => false;
    this.getRecette();
    this.getImage();
  }
}
