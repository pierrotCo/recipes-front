import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-test-component',
  templateUrl: './test-component.component.html',
  styleUrls: ['./test-component.component.css']
})
export class TestComponentComponent implements OnInit {

  constructor() { }

  nom = "l'OVNI !";
  @Input() approbationText: string;

  couleur="btn btn-success";

  clique(){
    if(this.couleur=="btn btn-success"){
      this.couleur="btn btn-danger";
    }else{
      this.couleur="btn btn-success";
    }
  }
  ngOnInit() {
  }

}
