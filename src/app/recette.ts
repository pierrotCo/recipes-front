class Ingredient{

   constructor(n:string,q:number){
    this.nom=n;
    this.quantite=q;
   } 
    nom:string;
    quantite:number

    changeQuantite(nouvelleQuantite){
        this.quantite=nouvelleQuantite;
    }
    
}


export class Recette{
    constructor(n:string,di:number,o:string,i:any[],v:boolean,de:string,coms:any[]){
        this.nom=n;
        this.url=this.genereLien(n);
        this.difficulte=di;
        this.origine=o;
        this.ingredients=[];
        this.vegan=v;
        this.description=de;
        this.commentaires=coms;
        for(let ingredient of i){
            this.ingredients.push(new Ingredient(ingredient.nom,ingredient.quantite));
        }
    }
    nom:string;
    url:string;
    vegan:boolean;
    difficulte:number;
    origine:string;
    ingredients:Ingredient[];
    description:string;
    commentaires:any[];

    genereLien(nom){
        let s="";
        let accent=false;
        console.log(nom.toLowerCase());
        for(let lettre of nom.toLowerCase()){
            s+=lettre;
            if(lettre=="é" || lettre=="è" || lettre=="ê"){
                s=s.slice(0,-1);
                s+="e";
            }
            if(lettre=="à" || lettre=="â"){
                s=s.slice(0,-1);
                s+="a";
            }
            if(lettre=="ô"){
                s=s.slice(0,-1);
                s+="o";
            }
            if(lettre=="ù"){
                s=s.slice(0,-1);
                s+="o";
            }
            if(lettre=="î"){
                s=s.slice(0,-1);
                s+="i";
            }
            if(lettre==" "){
                s=s.slice(0,-1);
                s+="-";
            }
        }
        return s;
    }

    changeQuantites(nouvellesQuantites){
        for(let modification of nouvellesQuantites){
            for(let ingredient of this.ingredients){
                if(modification.nomIngredient==ingredient.nom){
                    ingredient.changeQuantite(modification.nouvelleQuantite);
                }
            }
        }
    }

    addIngredient(ingredientNom,quantite){
        let newIngredient=new Ingredient(ingredientNom,quantite);
        this.ingredients.push(newIngredient)
    }

    removeIngredient(i){
        this.ingredients.splice(i,1);
        console.log(this.ingredients);
    }

    displayIngredients(){
        let liste=[];
        for(let ingredient of this.ingredients){
            liste.push({nom:ingredient.nom,quantite:ingredient.quantite});
        }
        return liste;
    }
}