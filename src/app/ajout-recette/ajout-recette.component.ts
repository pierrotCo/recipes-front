import { Component, OnInit } from '@angular/core';
import { RequestService } from "../service/request";




@Component({
  selector: 'app-ajout-recette',
  templateUrl: './ajout-recette.component.html',
  styleUrls: ['./ajout-recette.component.css']
})
export class AjoutRecetteComponent implements OnInit {

  array=[0];
  validated=false;
  nouvelleRecette={nom:"",difficulte:0,origine:"",ingredients:[{nom:"",quantite:0}],vegan:false,description:""};

  addIngredient(){
    this.array.push(this.array[this.array.length-1]+1);
    this.nouvelleRecette.ingredients.push({nom:"",quantite:0});
  }
  ingredient(i:number){
    return("ingredient"+i);
  }
  quantite(i:number){
    return("quantite"+i);
  }

  valide(){
    this.validated=true;
  }



  onSubmit(){
    if (this.validated){
      return this.requestService.ajouteRecette(this.nouvelleRecette).subscribe((data:any[])=> {
      });
    }
  }

  constructor(private requestService: RequestService) { }

  ngOnInit() {
  }

}
