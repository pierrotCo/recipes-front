import { Component } from '@angular/core';
import { RequestService } from "./service/request";




@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Mon livre de cuisine';
  nom = "";
  logged:boolean;
  message = "";

  generateLink(){
  }


  constructor(private requestService:RequestService) { }

  ngOnInit() {
    this.nom=this.requestService.user.nom;
    this.logged=this.requestService.loggedIn;
  }
}
