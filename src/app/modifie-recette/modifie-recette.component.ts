import { Component, OnInit, Input } from '@angular/core';
import { RequestService } from "../service/request";
import { Recette } from '../recette';


@Component({
  selector: 'app-modifie-recette',
  templateUrl: './modifie-recette.component.html',
  styleUrls: ['./modifie-recette.component.css']
})

export class ModifieRecetteComponent implements OnInit {

  @Input() recette:Recette;
  array=[0];
  validated=false;

  addArray(){
    this.array.push(this.array[this.array.length-1]+1);
  }
  addIngredient(){
    this.addArray()
    this.recette.addIngredient("",0);
  }

  ingredient(i:number){
    return("ingredient"+i);
  }
  quantite(i:number){
    return("quantite"+i);
  }

  supprimeIngredient(i){
    this.recette.removeIngredient(i);
    this.array.pop();
  }

  valide(){
    this.validated=true;
  }



  onSubmit(){
    if (this.validated){
      return this.requestService.modifieRecette(this.recette.url,this.recette).subscribe((data:any[])=> {
      });
    }
  }

  constructor(private requestService: RequestService) { }

  ngOnInit() {
    for(let ingredient of this.recette.ingredients){
      this.addArray();
    }
    this.array.pop();
  }

}
