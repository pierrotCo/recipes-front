import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifieRecetteComponent } from './modifie-recette.component';

describe('ModifieRecetteComponent', () => {
  let component: ModifieRecetteComponent;
  let fixture: ComponentFixture<ModifieRecetteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifieRecetteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifieRecetteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
