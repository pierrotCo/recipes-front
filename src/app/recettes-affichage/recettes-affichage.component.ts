import { Component, OnInit } from '@angular/core';
import { RequestService } from "../service/request";


@Component({
  selector: 'app-recettes-affichage',
  templateUrl: './recettes-affichage.component.html',
  styleUrls: ['./recettes-affichage.component.css']
})
export class RecettesAffichageComponent implements OnInit {
  recettes:any[];
  nom="";
  recettesTestees=[];

  getRecettes() {
    this.requestService.getRecettes().subscribe((data:any[]) => {
      this.recettes=data;
    });
  }

  constructor(private requestService:RequestService) { 
   }

  ngOnInit() {
    this.nom=this.requestService.user.nom;
    this.recettesTestees=this.requestService.user.recettesTestees;
    this.getRecettes();
  }

}
