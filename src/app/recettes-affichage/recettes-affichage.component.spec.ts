import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecettesAffichageComponent } from './recettes-affichage.component';

describe('RecettesAffichageComponent', () => {
  let component: RecettesAffichageComponent;
  let fixture: ComponentFixture<RecettesAffichageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecettesAffichageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecettesAffichageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
