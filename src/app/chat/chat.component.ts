import { Component, OnInit } from '@angular/core';
import { ChatService } from "../service/chat.service"; 
import { RequestService } from '../service/request';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private chatService : ChatService, private requestService : RequestService) { }

  message="";
  messages=[];

  ngOnInit() {
    this.chatService.getMessages().subscribe( (obj) => {
      this.messages.push(obj);
    });
  }

  sendMessage(){
    this.chatService.sendMessage(this.message, this.requestService.user.nom);
  }


}
