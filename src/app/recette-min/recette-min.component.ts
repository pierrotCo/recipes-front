import { Component, OnInit,Input } from '@angular/core';
import {Recette} from "../recette";
import { RequestService } from "../service/request";

@Component({
  selector: 'app-recette-min',
  templateUrl: './recette-min.component.html',
  styleUrls: ['./recette-min.component.css']
})
export class RecetteMinComponent implements OnInit {
  @Input() recetteJson: any;
  recette:Recette;


  supprime(){
    this.requestService.supprimeRecette(this.recette.nom).subscribe((data:any[]) => {
    });

  }

  createRecette(json){
    return new Recette(json.nom,json.difficulte,json.origine,json.ingredients,json.vegan,json.description,[]);
  }

  genereLien(nom){
    let s="";
    for(let lettre of nom.toLowerCase()){
        s+=lettre;
        if(lettre=="é" || lettre=="è" || lettre=="ê"){
            s=s.slice(0,-1);
            s+="e";
        }
        if(lettre=="à" || lettre=="â"){
            s=s.slice(0,-1);
            s+="a";
        }
        if(lettre=="ô"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="ù"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="î"){
            s=s.slice(0,-1);
            s+="i";
        }
        if(lettre==" "){
            s=s.slice(0,-1);
            s+="-";
        }
    }
    return s;
}

  constructor(private requestService:RequestService) { 
  }

  ngOnInit() {
    this.recette=this.createRecette(this.recetteJson);
  }
}