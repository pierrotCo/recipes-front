import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecetteMinComponent } from './recette-min.component';

describe('RecetteMinComponent', () => {
  let component: RecetteMinComponent;
  let fixture: ComponentFixture<RecetteMinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecetteMinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecetteMinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
